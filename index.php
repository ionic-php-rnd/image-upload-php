<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Headers: Content-Type");
$files = $_FILES['images'];

function get_folder_name()
{
    $foldername = "images/";
    if (!is_dir($foldername)) {
        mkdir($foldername);
    }
    return $foldername;
}

function isArray()
{
    global $files;
    $isArray = false;
    foreach ($files['name'] as $key => $value) {
        $isArray = true;
    }
    return $isArray;
}
if (isset($files)) {

    if (isArray()) {
        foreach ($files['name'] as $key => $value) {
            $target_file_path = get_folder_name() . $value;
            if (move_uploaded_file($_FILES["images"]["tmp_name"][$key], $target_file_path)) {
                $data[] = [
                    'status' => true,
                    'message' => $value . " is uploaded successfully.",
                    'path' => 'http://localhost/image_upload_api/' . $target_file_path
                ];
            }
        }
    } else {
        $target_file_path = get_folder_name() . $files['name'];
        if (move_uploaded_file($_FILES["images"]["tmp_name"], $target_file_path)) {
            $data = [
                'status' => true,
                'message' => $files['name'] . " is uploaded successfully.",
                'path' => 'http://localhost/image_upload_api/' . $target_file_path
            ];
        }
    }
    echo json_encode($data);
} else {
    echo json_encode([
        'status' => false,
        'message' => "Key is missing",
    ]);
}
